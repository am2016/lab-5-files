#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int find_cheapest(int *ar_price, int i); // prototype for finding cheapest.
int find_costliest(int *ar_price, int i);// prototype for finding costliest.
int find_average(int *ar_price, int i);//prototype for finding average.

int const ADDR_WIDTH = 40;
int const HOMES = 10000;
s
/*
 *Filename : homeprice.c
 *function: main
 *input: none
 *output: none
 *description: provides cheapest, costliest and avearage price of home from .csv files.
 *written by: Anil Mishra for Lan5:Files assignment for CS46:43521
*/
int main()
{
    int zip, id;
    char addr[ADDR_WIDTH];
    int price, bed, bath, sqf;
    FILE *fp;
    fp = fopen ("homes200.csv", "r");      //open file , read only
    //fp = fopen ("homelistings.csv", "r");      //open file , read only
    if (!fp) 
    {
        fprintf (stderr, "failed to open file for reading\n");
        return 1;
    }
    int i = 0;
    int *ar_zip = malloc(HOMES * sizeof(int));
    int *ar_id = malloc (HOMES * sizeof(int));
    char ar_addr[HOMES][ADDR_WIDTH];
    int *ar_price = malloc(HOMES * sizeof(int));
    int *ar_bed = malloc(HOMES * sizeof(int));
    int *ar_bath = malloc(HOMES * sizeof(int));
    int *ar_sqf = malloc(HOMES * sizeof(int));

    while (fscanf(fp, " %d, %d,%[^,],%d,%d,%d,%d",
                        &zip,&id,addr,&price, &bed,&bath, &sqf) != EOF)
    {
	ar_zip[i] = zip;
	ar_id[i] = id;
	ar_price[i] = price;
	ar_bed[i] = bed;
	ar_bath[i] = bath;
	ar_sqf[i] = sqf;
        strncpy(ar_addr[i],addr,ADDR_WIDTH);
	i++;
    } 
    int cheapest = find_cheapest(ar_price, i);
    int costliest = find_costliest(ar_price, i);
    int average = find_average(ar_price, i); 

    printf("%d %d %d \n",cheapest, costliest, average);	
    fclose(fp);
}

int find_cheapest(int *ar_price, int i)
{
    int min = ar_price[0];
    for ( int j = 0; j < i; j++)
    {
	if (ar_price[j] < min)
        {
	     min = ar_price[j];
        }
    }
    return min;
}

int find_costliest(int *ar_price, int i)
{
    int max = ar_price[0];
    for (int j = 0; j < i; j++)
    {
	if (ar_price[j] > max)
        {
	     max = ar_price[j];
        }
    }
    return max;
}

int find_average(int *ar_price, int i)
{
    int average = 0;
    long long sum = 0;
    for (int j = 0; j < i; j++)
    {
        sum +=ar_price[j];
    }
    average = sum/i;
    return average;
}
