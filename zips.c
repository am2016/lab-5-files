#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int const ADDR_WIDTH = 40;
int const HOMES = 10000;
/*
 *Filename : zips.c
 *function: main
 *input: none
 *output: none
 *description: arranges the file as per zipcode.
 *written by: Anil Mishra for Lan5:Files assignment for CS46:43521
*/

int main()
{
    int zip, id;
    char addr[ADDR_WIDTH];
    int price, bed, bath, sqf;
    FILE *fp;
    fp = fopen ("homelistings.csv", "r");      //open file , read only
    if (!fp) 
    {
        fprintf (stderr, "failed to open file for reading\n");
        return 1;
    }
    int i = 0;
    int *ar_zip = malloc(HOMES * sizeof(int));
    int *ar_id = malloc (HOMES * sizeof(int));
    char ar_addr[HOMES][ADDR_WIDTH];
    int *ar_price = malloc(HOMES * sizeof(int));
    int *ar_bed = malloc(HOMES * sizeof(int));
    int *ar_bath = malloc(HOMES * sizeof(int));
    int *ar_sqf = malloc(HOMES * sizeof(int));

    while (fscanf(fp, " %d, %d,%[^,],%d,%d,%d,%d",
                        &zip,&id,addr,&price, &bed,&bath, &sqf) != EOF)
    {
	ar_zip[i] = zip;
	ar_id[i] = id;
	ar_price[i] = price;
	ar_bed[i] = bed;
	ar_bath[i] = bath;
	ar_sqf[i] = sqf;
        strncpy(ar_addr[i],addr,ADDR_WIDTH);
	i++;
    } 
    int zip_d[HOMES][2] = {0}; 
    int zcode; 
    int zfreq;
    int zsum=0;
    for ( int k = 0; k < i; k++)
    {
         zcode = ar_zip[k];
         zfreq = 0;
         int found = 0;
         for (int l = 0; l < zsum; l++)
         {
	      if (ar_zip[k] == zip_d[l][0])
	      {
                     found = 1;
              }
          }
          for (int j = 0; j < i; j++)
          {
               if ((ar_zip[k] == ar_zip[j]) && (found ==0))
	       {
		    zfreq++;
	       }
           }
           if (found == 0)
           {
                zip_d[k][0] = ar_zip[k];
                zip_d[k][1] = zfreq;
                zsum +=zfreq;
           }
      }
      char files[zsum][5];
      char fname[10];
      FILE *f;
      for (int m =0; m <zsum; m++)
      {
           if (zip_d[m][0])
           {
                    sprintf(files[m], "%d", zip_d[m][0]);
                    strcpy(fname, files[m]);
                    strcat(fname,".txt");
                    f = fopen(fname,"w");
                    if (!f)
                    {
                         printf("Could not open file %si\n",fname);
                         exit(1);
                    }
                    for (int j =0; j < HOMES; j++)
                    {
                    	if (zip_d[m][0] == ar_zip[j])
                    	{
	               	    fprintf(f,"%6d : %40s\n",
                              ar_zip[j], ar_addr[j]);
                        }
                    }
                    fclose(f);
                    
             }
             
       }    
      fclose(fp);
}

