#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int const SMALL = 1000;
int const MED = 2000;
int const ADDR_WIDTH = 40;
int const HOMES = 10000;

/*
 *Filename : sml.c
 *function: main
 *input: none
 *output: none
 *description: creates the file for small, medium, large sqf home.
 *written by: Anil Mishra for Lan5:Files assignment for CS46:43521
*/
int main()
{
    int zip, id;
    char addr[ADDR_WIDTH];
    int price, bed, bath, sqf;
    FILE *fp;
    fp = fopen ("homelistings.csv", "r");      //open file , read only
    if (!fp) 
    {
        fprintf (stderr, "failed to open file for reading\n");
        return 1;
    }
    int i = 0;
    int *ar_zip = malloc(HOMES * sizeof(int));
    int *ar_id = malloc (HOMES * sizeof(int));
    char ar_addr[HOMES][ADDR_WIDTH];
    int *ar_price = malloc(HOMES * sizeof(int));
    int *ar_bed = malloc(HOMES * sizeof(int));
    int *ar_bath = malloc(HOMES * sizeof(int));
    int *ar_sqf = malloc(HOMES * sizeof(int));

    int *sr_zip = malloc(HOMES * sizeof(int));
    int *sr_id = malloc (HOMES * sizeof(int));
    char sr_addr[HOMES][ADDR_WIDTH];
    int *sr_price = malloc(HOMES * sizeof(int));
    int *sr_bed = malloc(HOMES * sizeof(int));
    int *sr_bath = malloc(HOMES * sizeof(int));
    int *sr_sqf = malloc(HOMES * sizeof(int));

    int *md_zip = malloc(HOMES * sizeof(int));
    int *md_id = malloc (HOMES * sizeof(int));
    char md_addr[HOMES][ADDR_WIDTH];
    int *md_price = malloc(HOMES * sizeof(int));
    int *md_bed = malloc(HOMES * sizeof(int));
    int *md_bath = malloc(HOMES * sizeof(int));
    int *md_sqf = malloc(HOMES * sizeof(int));

    int *lg_zip = malloc(HOMES * sizeof(int));
    int *lg_id = malloc (HOMES * sizeof(int));
    char lg_addr[HOMES][ADDR_WIDTH];
    int *lg_price = malloc(HOMES * sizeof(int));
    int *lg_bed = malloc(HOMES * sizeof(int));
    int *lg_bath = malloc(HOMES * sizeof(int));
    int *lg_sqf = malloc(HOMES * sizeof(int));

    while (fscanf(fp, " %d, %d,%[^,],%d,%d,%d,%d",
                        &zip,&id,addr,&price, &bed,&bath, &sqf) != EOF)
    {
	ar_zip[i] = zip;
	ar_id[i] = id;
	ar_price[i] = price;
	ar_bed[i] = bed;
	ar_bath[i] = bath;
	ar_sqf[i] = sqf;
        strncpy(ar_addr[i],addr,ADDR_WIDTH);
	i++;
    }
    int si = 0;
    for (int j = 0; j < i; j++)
    {
         if (ar_sqf[j] < SMALL)
         {
              sr_zip[si] = ar_zip[j];
              sr_id[si] = ar_id[j];
              strncpy(sr_addr[si],ar_addr[j],ADDR_WIDTH);
              sr_price[si] = ar_price[j];
              sr_bed[si] = ar_bed[j];
              sr_bath[si] = ar_bath[j];
              sr_sqf[si] = ar_sqf[j];
              si++;
           }
    }
    FILE *sf;    
    sf = fopen("small.txt","w");
    if (!sf) 
    {
        fprintf (stderr, "failed to open file for reading\n");
        return 1;
    }
    
    for (int j = 0 ; j < si; j++)
    {
	fprintf(sf,"%30s : %10d\n",
               sr_addr[j], sr_sqf[j]);

    }

    fclose(sf);
   
// medium stuff. 
    int mi = 0;
    for (int j = 0; j < i; j++)
    {
         if ((ar_sqf[j] > SMALL)&&(ar_sqf[j]< MED))
         {
              md_zip[mi] = ar_zip[j];
              md_id[mi] = ar_id[j];
              strncpy(md_addr[mi],ar_addr[j],ADDR_WIDTH);
              md_price[mi] = ar_price[j];
              md_bed[mi] = ar_bed[j];
              md_bath[mi] = ar_bath[j];
              md_sqf[mi] = ar_sqf[j];
              mi++;
           }
    }
    FILE *mf;    
    mf = fopen("medium.txt","w");
    if (!mf) 
    {
        fprintf (stderr, "failed to open file for reading\n");
        return 1;
    }
    
    for (int j = 0 ; j < mi; j++)
    {
	fprintf(mf,"%30s : %10d\n",
               md_addr[j], md_sqf[j]);

    }

    fclose(mf);
   
  //large stuff 
    int li = 0;
    for (int j = 0; j < i; j++)
    {
         if ((ar_sqf[j] > MED))
         {
              lg_zip[li] = ar_zip[j];
              lg_id[li] = ar_id[j];
              strncpy(lg_addr[li],ar_addr[j],ADDR_WIDTH);
              lg_price[li] = ar_price[j];
              lg_bed[li] = ar_bed[j];
              lg_bath[li] = ar_bath[j];
              lg_sqf[li] = ar_sqf[j];
              li++;
           }
    }
    FILE *lf;    
    lf = fopen("large.txt","w");
    if (!lf) 
    {
        fprintf (stderr, "failed to open file for reading\n");
        return 1;
    }
  
    for (int j = 0 ; j < li; j++)
    {
	fprintf(lf,"%30s : %10d\n",
               lg_addr[j], lg_sqf[j]);

    }

    fclose(lf);

    fclose(fp);
}
